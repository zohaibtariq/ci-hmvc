<?php
/**
 * Created by Zohaib Tariq (mr.zohaibt@gmail.com)
 * Date: 4/4/2017
 */
class Dashboard extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$data["content_view"] = "Dashboard/dashboard1_v";
		$this->template->admin_template($data);
	}
}