<?php
/**
 * Created by Zohaib Tariq (mr.zohaibt@gmail.com)
 * Date: 4/4/2017
 */
class Home extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
//		$user_id = 1;
//		$this->load->module('module_name');
//		$this->module_name->modules_method_name($user_id);﻿
//		Modules::run('module_name/modules_method_name', $user_id);
//		$this->load->model("module_name/modules_method_name");
//		https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc
	}
	function index(){
		$data["content_view"] = "home/home";
		$this->template->sample_template($data);
	}
	function about(){
		$data["content_view"] = "home/about";
		$this->template->sample_template($data);
	}
}