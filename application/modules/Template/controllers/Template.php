<?php
/**
 * Created by Zohaib Tariq (mr.zohaibt@gmail.com)
 * Date: 4/4/2017
 */
class Template extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
	}
	function index(){
		die("hello from ci hmvc template");
	}
	function sample_template($data = null){
		$this->load->view("Template/sample_template_v", $data);
	}
	function admin_template($data = null){
		$this->load->view("Template/admin_template_v", $data);
	}
}